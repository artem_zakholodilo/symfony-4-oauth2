<?php

namespace App\Command;

use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends ContainerAwareCommand
{
    /**
     * @var ClientManagerInterface $clientManger
     */
    public $clientManger;

    public function __construct(ClientManagerInterface $clientManager)
    {
        $this->clientManager = $clientManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('oauth-server:user:create')
            ->setDescription('Create oauth user')
            ->addOption(
                'redirect-uri',
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'Set redirect uri',
                null
            )
            ->addOption(
                'grant-type',
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'Set user grant',
                null
            )
            ->setHelp(
                <<<EOT
                    The <info>%command.name%</info>command creates a new user.

<info>php %command.full_name% [--redirect-uri=...] [--grant-type=...] name</info>

EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = $this->clientManager->createClient();
        $user->setRedirectUris($input->getOption('redirect-uri'));
        $user->setAllowedGrantTypes($input->getOption('grant-type'));
        $this->clientManager->updateClient($user);
        $output->writeln(
            sprintf(
                'Added a new client with public id <info>%s</info>, secret <info>%s</info>',
                $user->getPublicId(),
                $user->getSecret()
            )
        );
    }

}
