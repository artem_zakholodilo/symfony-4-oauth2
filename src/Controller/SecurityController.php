<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Users;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class SecurityController extends Controller
{
    /**
     * Login user by token
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        var_dump($request); exit;
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            $error = $error->getMessage();
        }

        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        return $this->render(
            'AcmeDemoBundle:Security:login.html.twig',
            array(
                'last_username' => $lastUsername,
                'error' => $error,
            )
        );
    }

    public function loginCheckAction(Request $request)
    {
        var_dump($request); exit;
        $tokenManager = $this->get('fos_oauth_server.access_token_manager.default');

        $bearerToken = $this->get('fos_oauth_server.server')->getBearerToken($request);
        if (!$bearerToken) {
            return new JsonResponse(['message' => 'Bearer token not supplied'], 400);
        }

        $accessToken = $tokenManager->findTokenByToken($bearerToken);

        if (!$accessToken) {
            return new JsonResponse(['message' => 'Bearer token not valid'], 400);
        }

        if ($accessToken->hasExpired()) {
            return new JsonResponse(['message' => 'Access token has expired'], 400);
        }

        // may want to validate something else about the client, but that is beyond OAuth2 scope
        //$client = $accessToken->getClient();

        return null;
    }

    public function registration(Request $request, ObjectManager $manager)
    {
        $user = new User();
        $password = sha1($request->request->get('password') ?? 'password');
        $user->setPassword($password);
        $user->setUsername($request->request->get('firstName') ?? 'lol@gmail.com');

        $manager->persist($user);
        $manager->flush();

        return new JsonResponse(null, 201);
    }
}