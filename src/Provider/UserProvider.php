<?php

namespace App\Provider;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\NoResultException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class UserProvider extends ServiceEntityRepository implements UserProviderInterface
{
    /**
     * @var ObjectRepository
     */
    private $userRepository;

    /**
     * UserProvider constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        $this->userRepository = $registry->getEntityManager();
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $username
     * @throws NoResultException
     * @return UserInterface
     */
    public function loadUserByUsername($username)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery();

        try {
            $user = $q->getSingleResult();
        } catch (NoResultException $ex) {
            $message = sprintf(
                'Unable to find an active admin User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $ex);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->userRepository->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->userRepository->getClassName() === $class
            || is_subclass_of($class, $this->userRepository->getClassName());
    }
}